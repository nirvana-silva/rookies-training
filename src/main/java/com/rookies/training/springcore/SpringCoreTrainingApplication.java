package com.rookies.training.springcore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCoreTrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCoreTrainingApplication.class, args);
	}

}
